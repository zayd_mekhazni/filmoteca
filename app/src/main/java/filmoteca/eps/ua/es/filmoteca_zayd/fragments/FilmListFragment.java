package filmoteca.eps.ua.es.filmoteca_zayd.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.AboutActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.LogInActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.TokenActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.WebActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.adapters.FilmArrayAdapter;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.ObjectStreamManager;

public class FilmListFragment extends ListFragment {

    private ListView listView_list;
    private FilmArrayAdapter adaptador;

    private GoogleSignInClient mGoogleSignInClient;

    private OnItemSelectedListener mCallBack;
    public interface OnItemSelectedListener{
        void onItemSelectedListener(int position);
        void onItemDeletedListener();
    }

    /* Constructor vacio necesario. */
    public FilmListFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Activar el boton para volver a la pantalla principal. */
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity)getActivity()). getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setHasOptionsMenu(true);

        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_film_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Asignar referencias de vistas. */
        listView_list = getListView();

        /* Inicializar adaptador. */
        adaptador = new FilmArrayAdapter(getContext(), R.layout.film_item_list, FilmDataSource.films);
        final int[] count = {0};
        listView_list.setAdapter(adaptador);

        /* Establecer el modo CHOICE_MODE_MULTIPLE_MODAL en el ListView, y su respectivo listener. */
        listView_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView_list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            /* Cuando se seleccione o se deseleccione un elemento, actualizar el contador. */
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if(checked){
                    count[0]++;
                }else{
                    count[0]--;
                }
                mode.setTitle(count[0] + getString(R.string.selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_contextual,menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            /* Cuando se pulsa el boton de Borrar, eliminar todos los elementos seleccionados, y guardar los cambios en el fichero. */
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                SparseBooleanArray selected = listView_list.getCheckedItemPositions();
                switch (item.getItemId()){
                    case R.id.action_delete:

                        for(int i =(selected.size() - 1); i >= 0; i--){
                            if(selected.valueAt(i)){
                                Film selectedFilm = adaptador.getItem(selected.keyAt(i));
                                adaptador.remove(selectedFilm);
                            }
                        }
                        /* Escritura de ficheros, sincrona y asincrona. */
                        //ObjectStreamManager.grabarDatosFichero(getContext());
                        new ObjectStreamManager.grabarDatosFicheroAsincrono(getContext()).execute();
                        ((BaseAdapter) listView_list.getAdapter()).notifyDataSetChanged();
                        count[0] = 0;
                        mCallBack.onItemDeletedListener();
                        mode.finish();
                        return true;
                    case R.id.tweet:
                        String[] favoriteFilms = new String[selected.size()];
                        for(int i =(selected.size() - 1); i >= 0; i--){
                            if(selected.valueAt(i)){
                                Film selectedFilm = adaptador.getItem(selected.keyAt(i));
                                favoriteFilms[i] = selectedFilm.getTitle();
                            }
                        }
                        twitterTweet(favoriteFilms);
                        mode.finish();
                        return true;
                    case R.id.addGeofence:
                        for(int i =(selected.size() - 1); i >= 0; i--){
                            if(selected.valueAt(i)){
                                Film film = FilmDataSource.films.get(selected.keyAt(i));
                                film.setGeofence(true);
                            }
                        }
                        mode.finish();
                        return true;
                    case R.id.removeGeofence:
                        for(int i =(selected.size() - 1); i >= 0; i--){
                            if(selected.valueAt(i)){
                                Film film = FilmDataSource.films.get(selected.keyAt(i));
                                film.setGeofence(false);
                            }
                        }
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                count[0] = 0;
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCallBack.onItemSelectedListener(position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /* Comprueba que la actividad implemente la interfaz definida. */
        try{
            mCallBack = (OnItemSelectedListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " debe implementar onItemSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.menu_settings,menu);
    }

    /* Comprobamos que elemento del menu fue seleccionado. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (R.id.menu_settings01):
                Film film = new Film();
                film.setTitle("Prueba");
                film.setDirector("Prueba");
                film.setYear(1111);
                film.setImdbUrl("http://www.google.es");
                film.setComments("Prueba");
                film.setGenre(Film.GENRE_SCIFI);
                film.setFormat(Film.FORMAT_DVD);
                film.setImageResId(R.mipmap.ic_launcher);
                FilmDataSource.films.add(film);
                ((BaseAdapter) listView_list.getAdapter()).notifyDataSetChanged();

                /* Lectura de ficheros, sincrona y asincrona. */
                //ObjectStreamManager.grabarDatosFichero(getContext());
                new ObjectStreamManager.grabarDatosFicheroAsincrono(getContext()).execute();
                return true;
            case (R.id.menu_settings02):
                startActivity(new Intent(getActivity(),AboutActivity.class));
                return true;
            case (R.id.menu_settings03):
                mGoogleSignInClient.signOut();
                startActivity(new Intent(getActivity(), LogInActivity.class));
                return true;
            case (R.id.menu_settings04):
                startActivity(new Intent(getActivity(), TokenActivity.class));

        }
        /* Devolvemos false si no hemos hecho nada con el elemento
           seleccionado (permitira que se ejecuten otros manejadores)
           Si devolvemos true finalizara el procesamiento. */
        return false;
    }

    public void twitterTweet(String[] films){
        Intent twitterIntent = new Intent(getContext(), WebActivity.class);
        twitterIntent.putExtra("favoriteFilms", films);
        startActivity(twitterIntent);
    }

}
