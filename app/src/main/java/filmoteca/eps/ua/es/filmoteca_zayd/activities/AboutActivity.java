package filmoteca.eps.ua.es.filmoteca_zayd.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import filmoteca.eps.ua.es.filmoteca_zayd.R;

public class AboutActivity extends AppCompatActivity {

    Button button_irSitioWeb;
    Button button_obtenerSoporte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        /* Activar el boton para volver a la pantalla principal. */
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /* Asignar referencias de vistas. */
        button_irSitioWeb = findViewById(R.id.button_irSitioWeb);
        button_obtenerSoporte = findViewById(R.id.button_obtenerSoporte);

        /* Listener del boton Ir al Sitio Web. */
        button_irSitioWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.google.es";
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(webIntent);
            }
        });

        /* Listener del boton Obtener Soporte. */
        button_obtenerSoporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:mae32@alu.ua.es"));
                startActivity(emailIntent);
            }
        });
    }

    /* Cuando se pulsa el boton de volver, volver a la actividad principal. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (android.R.id.home):
                NavUtils.navigateUpTo(AboutActivity.this,
                        new Intent(AboutActivity.this, FragmentContainerActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(elemento);
    }
}
