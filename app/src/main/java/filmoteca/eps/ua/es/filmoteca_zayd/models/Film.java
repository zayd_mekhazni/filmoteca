package filmoteca.eps.ua.es.filmoteca_zayd.models;

import java.io.Serializable;
import java.util.UUID;

/* Clase para representar las peliculas. */
public class Film implements Serializable {

    /* Formatos */
    public final static int FORMAT_DVD = 0;
    public final static int FORMAT_BLURAY = 1;
    public final static int FORMAT_DIGITAL = 2;

    /* Generos */
    public final static int GENRE_ACTION = 3;
    public final static int GENRE_COMEDY = 4;
    public final static int GENRE_DRAMA = 5;
    public final static int GENRE_SCIFI = 6;
    public final static int GENRE_HORROR = 7;

    private int imageResId;
    private String title;
    private String director;
    private int year;
    private int genre;
    private int format;
    private String imdbUrl;
    private String comments;
    private double lat;
    private double lng;
    private boolean isGeofence;
    private final String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public boolean isGeofence() {
        return isGeofence;
    }

    public void setGeofence(boolean geofence) {
        isGeofence = geofence;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getGenre() {
        return genre;
    }

    public void setGenre(int genre) {
        this.genre = genre;
    }

    public int getFormat() {
        return format;
    }

    public void setFormat(int format) {
        this.format = format;
    }

    public String getImdbUrl() {
        return imdbUrl;
    }

    public void setImdbUrl(String imdbUrl) {
        this.imdbUrl = imdbUrl;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String toString(){
        return this.title;
    }

}
