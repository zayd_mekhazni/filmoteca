package filmoteca.eps.ua.es.filmoteca_zayd.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;

/* Clase adaptador para el Recyclerview de peliculas. */
public class FilmArrayRecyclerAdapter extends RecyclerView.Adapter<FilmArrayRecyclerAdapter.FilmArrayViewHolder> {

    public interface onItemClickListener{
        void onItemClick(Film film,int position);
    }

    private onItemClickListener mListener;
    private ArrayList<Film> films;

    public void setOnItemKistener(onItemClickListener listener){
        mListener = listener;
    }

    /* Inicializar la clase: Asignar referencias de vistas. */
    class FilmArrayViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView_filmlist;
        private TextView textView_filmlisttitle;
        private TextView textView_listitemdirector;

        FilmArrayViewHolder(View itemView) {
            super(itemView);
            this.imageView_filmlist = itemView.findViewById(R.id.imageView_filmlist);
            this.textView_filmlisttitle = itemView.findViewById(R.id.textView_filmlisttitle);
            this.textView_listitemdirector = itemView.findViewById(R.id.textView_listitemdirector);
        }
    }

    /* Constructor de la clase. */
    public FilmArrayRecyclerAdapter(ArrayList<Film> films) {
        this.films = films;
    }

    /* Inflar los elementos de la lista, y enlazar el listener de cada elemento. */
    @NonNull
    @Override
    public FilmArrayRecyclerAdapter.FilmArrayViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.film_item_list, parent, false);
        final FilmArrayViewHolder holder = new FilmArrayViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if(mListener!=null){
                    mListener.onItemClick(films.get(position),position);
                }
            }
        });
        return holder;
    }

    /* Enlazar los datos de la pelicula con la vista. */
    @Override
    public void onBindViewHolder(@NonNull FilmArrayViewHolder holder, int position) {
        Film film = films.get(position);
        holder.imageView_filmlist.setImageResource(film.getImageResId());
        holder.textView_filmlisttitle.setText(film.getTitle());
        holder.textView_listitemdirector.setText(film.getDirector());
    }

    @Override
    public int getItemCount() {
        return films.size();
    }
}
