package filmoteca.eps.ua.es.filmoteca_zayd.thrash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.AboutActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.adapters.FilmArrayRecyclerAdapter;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

public class FilmListRecyclerActivity extends AppCompatActivity {

    RecyclerView recyclerView_ListaPeliculas;
    LinearLayoutManager recyclerView_Manager;
    FilmArrayRecyclerAdapter recyclerView_Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list_recycler);

        /* Asignar referencias de vistas. */
        recyclerView_ListaPeliculas = findViewById(R.id.recyclerView_ListaPeliculas);

        /* Initialize RecyclerView. */
        recyclerView_Manager = new LinearLayoutManager(this);
        recyclerView_Adapter = new FilmArrayRecyclerAdapter((ArrayList<Film>) FilmDataSource.films);
        recyclerView_ListaPeliculas.setLayoutManager(recyclerView_Manager);
        recyclerView_ListaPeliculas.setAdapter(recyclerView_Adapter);
        recyclerView_ListaPeliculas.setItemAnimator(new DefaultItemAnimator());

        /* Listener del RecyclerView. */
        recyclerView_Adapter.setOnItemKistener(new FilmArrayRecyclerAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Film film, int position) {
                Intent intent = new Intent(FilmListRecyclerActivity.this,FilmDataActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_POSITION,position);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_settings,menu);
        return true;
    }

    /* Comprobamos que elemento del menu fue seleccionado. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (R.id.menu_settings01):
                Film film = new Film();
                film.setTitle("Prueba");
                film.setDirector("Prueba");
                film.setYear(1111);
                film.setImdbUrl("http://www.google.es");
                film.setComments("Prueba");
                film.setGenre(Film.GENRE_SCIFI);
                film.setFormat(Film.FORMAT_DVD);
                film.setImageResId(R.mipmap.ic_launcher);
                FilmDataSource.films.add(film);
                recyclerView_Adapter.notifyDataSetChanged();
                return true;
            case (R.id.menu_settings02):
                startActivity(new Intent(FilmListRecyclerActivity.this,AboutActivity.class));
                return true;
        }
        // Devolvemos false si no hemos hecho nada con el elemento
        // seleccionado (permitira que se ejecuten otros manejadores)
        // Si devolvemos true finalizara el procesamiento.
        return false;
    }

}
