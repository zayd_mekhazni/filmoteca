package filmoteca.eps.ua.es.filmoteca_zayd.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;


/* Clase adaptador para la lista de peliculas. */
public class FilmArrayAdapter extends ArrayAdapter<Film> {

    public FilmArrayAdapter(@NonNull Context context, int resource, @NonNull List<Film> films) {
        super(context, resource, films);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.film_item_list,parent,false);
        }

        /* Asignar referencias de vistas. */
        ImageView imageView_filmlist = convertView.findViewById(R.id.imageView_filmlist);
        TextView textView_filmlisttitle = convertView.findViewById(R.id.textView_filmlisttitle);
        TextView textView_listitemdirector = convertView.findViewById(R.id.textView_listitemdirector);

        /* Enlazar los datos de la pelicula con la vista. */
        Film film = getItem(position);
        if (film != null) {
            imageView_filmlist.setImageResource(film.getImageResId());
            textView_filmlisttitle.setText(film.getTitle());
            textView_listitemdirector.setText(film.getDirector());
        }

        return convertView;
    }

}
