package filmoteca.eps.ua.es.filmoteca_zayd.thrash;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.FilmEditActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

public class FilmDataActivity extends AppCompatActivity {

    public static final String EXTRA_FILM_POSITION = "EXTRA_FILM_POSITION";

    ImageView imageView_Pelicula;
    TextView textView_TituloPelicula;
    TextView textView_Director2;
    TextView textView_Ano2;
    TextView textView_Descripcion;
    TextView textView_Notas;

    Button button_PeliculaRelacionada;
    Button button_EditarPelicula;
    Button button_VolverPrincipal;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_data);

        /* Activar el boton para volver a la pantalla principal. */
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /* Asignar referencias de vistas. */
        imageView_Pelicula = findViewById(R.id.imageView_Pelicula);
        textView_TituloPelicula = findViewById(R.id.textView_TituloPelicula);
        textView_Director2 = findViewById(R.id.textView_Director2);
        textView_Ano2 = findViewById(R.id.textView_Año2);
        textView_Descripcion = findViewById(R.id.textView_Descripcion);
        textView_Notas = findViewById(R.id.textView_Notas);

        button_PeliculaRelacionada = findViewById(R.id.button_VerIMDB);
        button_EditarPelicula = findViewById(R.id.button_EditarPelicula);
        button_VolverPrincipal = findViewById(R.id.button_VolverPrincipal);

        /* Actualizar los datos de la pelicula. */
        Intent intent = getIntent();
        final int posicionPelicula = intent.getIntExtra(FilmDataActivity.EXTRA_FILM_POSITION,0);
        textView_TituloPelicula.setText(FilmDataSource.films.get(posicionPelicula).getTitle());
        textView_Director2.setText(FilmDataSource.films.get(posicionPelicula).getDirector());
        textView_Ano2.setText(Integer.toString(FilmDataSource.films.get(posicionPelicula).getYear()));

        String formato = "";
        switch (FilmDataSource.films.get(posicionPelicula).getFormat()){
            case(Film.FORMAT_BLURAY):
                formato = getString(R.string.bluray);
                break;
            case(Film.FORMAT_DIGITAL):
                formato = getString(R.string.digital);
                break;
            case(Film.FORMAT_DVD):
                formato = getString(R.string.dvd);
                break;
        }

        String genero = "";
        switch (FilmDataSource.films.get(posicionPelicula).getGenre()){
            case(Film.GENRE_SCIFI):
                genero = getString(R.string.scifi);
                break;
            case(Film.GENRE_HORROR):
                genero = getString(R.string.horror);
                break;
            case(Film.GENRE_DRAMA):
                genero = getString(R.string.drama);
                break;
            case(Film.GENRE_COMEDY):
                genero = getString(R.string.comedy);
                break;
            case(Film.GENRE_ACTION):
                genero = getString(R.string.action);
                break;
        }

        textView_Descripcion.setText(formato + ", " + genero);
        textView_Notas.setText(FilmDataSource.films.get(posicionPelicula).getComments());
        imageView_Pelicula.setImageResource(FilmDataSource.films.get(posicionPelicula).getImageResId());


        /* Listener del boton Ver En IMDB. */
        button_PeliculaRelacionada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = FilmDataSource.films.get(posicionPelicula).getImdbUrl();
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(webIntent);
            }
        });

        /* Listener del boton Editar pelicula. */
        button_EditarPelicula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilmDataActivity.this,FilmEditActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_POSITION,posicionPelicula);
                startActivityForResult(intent,0);
            }
        });

        /* Listener del boton Volver a la Principal. */
        button_VolverPrincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilmDataActivity.this, FilmListRecyclerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this,getString(R.string.editacepted),Toast.LENGTH_LONG).show();
                int posicionPelicula = data.getIntExtra(FilmDataActivity.EXTRA_FILM_POSITION,0);
                Intent intent = new Intent(FilmDataActivity.this,FilmDataActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_POSITION,posicionPelicula);
                startActivity(intent);
                finish();
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this,getString(R.string.editcanceled),Toast.LENGTH_LONG).show();
            }
        }
    }

    /* Cuando se pulsa el boton de volver, volver a la actividad principal. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (android.R.id.home): // ID especial para el boton HOME
                NavUtils.navigateUpTo(FilmDataActivity.this,
                        new Intent(FilmDataActivity.this, FilmListRecyclerActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(elemento);
    }

}
