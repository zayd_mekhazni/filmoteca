//package filmoteca.eps.ua.es.filmoteca_zayd.utils;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.Geofence;
//
///**
// * Created by Zayd on 19/04/2019.
// */
//
//public class GeofenceController {
//
//    private final String TAG = GeofenceController.class.getName();
//
//    private Context context;
//    private GoogleApiClient googleApiClient;
//    private Gson gson;
//    private SharedPreferences prefs;
//
//    private List<NamedGeofence> namedGeofences;
//    public List<NamedGeofence> getNamedGeofences() {
//        return namedGeofences;
//    }
//
//    private List<NamedGeofence> namedGeofencesToRemove;
//
//    private Geofence geofenceToAdd;
//    private NamedGeofence namedGeofenceToAdd;
//    private static GeofenceController INSTANCE;
//
//    public static GeofenceController getInstance() {
//        if (INSTANCE == null) {
//            INSTANCE = new GeofenceController();
//        }
//        return INSTANCE;
//    }
//
//    public void init(Context context) {
//        this.context = context.getApplicationContext();
//
//        gson = new Gson();
//        namedGeofences = new ArrayList<>();
//        namedGeofencesToRemove = new ArrayList<>();
//        prefs = this.context.getSharedPreferences(Constants.SharedPrefs.Geofences, Context.MODE_PRIVATE);
//    }
//
//}
