package filmoteca.eps.ua.es.filmoteca_zayd.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.FilmEditActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.PositionActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FilmDataFragment extends android.support.v4.app.Fragment{

    public static final String EXTRA_FILM_POSITION = "EXTRA_FILM_POSITION";

    ImageView imageView_Pelicula;
    TextView textView_TituloPelicula;
    TextView textView_Director1;
    TextView textView_Director2;
    TextView textView_Ano1;
    TextView textView_Ano2;
    TextView textView_Descripcion;
    TextView textView_Notas;

    Button button_PeliculaRelacionada;
    Button button_EditarPelicula;
    Button button_VerPosicion;

    int posicionPelicula;

    /* Constructor vacio necesario. */
    public FilmDataFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Activar el boton para volver a la pantalla principal. */
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((AppCompatActivity)getActivity()). getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_film_data, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Asignar referencias de vistas. */
        imageView_Pelicula = view.findViewById(R.id.imageView_Pelicula);
        textView_TituloPelicula = view.findViewById(R.id.textView_TituloPelicula);
        textView_Director1 = view.findViewById(R.id.textView_Director1);
        textView_Director2 = view.findViewById(R.id.textView_Director2);
        textView_Ano1 = view.findViewById(R.id.textView_Año1);
        textView_Ano2 = view.findViewById(R.id.textView_Año2);
        textView_Descripcion = view.findViewById(R.id.textView_Descripcion);
        textView_Notas = view.findViewById(R.id.textView_Notas);

        button_PeliculaRelacionada = view.findViewById(R.id.button_VerIMDB);
        button_EditarPelicula = view.findViewById(R.id.button_EditarPelicula);
        button_VerPosicion = view.findViewById(R.id.showPos);

        /* Actualizar los datos de la pelicula. */
        if (getArguments() != null) {
            posicionPelicula = getArguments().getInt("position");
            actualizarDatosVista();
        }else{
            if(FilmDataSource.films.isEmpty()){
                posicionPelicula = -1;
                ocultarVista();
            }else{
                posicionPelicula = 0;
                actualizarDatosVista();
            }
        }

        if(Double.isNaN(FilmDataSource.films.get(posicionPelicula).getLat())  || Double.isNaN(FilmDataSource.films.get(posicionPelicula).getLng())){
            button_VerPosicion.setClickable(false);
        }else{
            button_VerPosicion.setClickable(true);
        }

        /* Listener del boton Ver En IMDB. */
        button_PeliculaRelacionada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = FilmDataSource.films.get(posicionPelicula).getImdbUrl();
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(webIntent);
            }
        });

        /* Listener del boton Editar pelicula. */
        button_EditarPelicula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),FilmEditActivity.class);
                intent.putExtra(FilmDataFragment.EXTRA_FILM_POSITION,posicionPelicula);
                startActivityForResult(intent,0);
            }
        });

        button_VerPosicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), PositionActivity.class);
                myIntent.putExtra(FilmDataFragment.EXTRA_FILM_POSITION,posicionPelicula);
                startActivity(myIntent);
            }
        });

    }

    /* Si la actividad de editar la pelicula devuelve RESULT_OK, notificar al usuario que los cambios se han efectuado con exito.
       Si la actividad de editar la pelicula devuelve RESULT_CANCELED, notiifcar al usuario que los cambios han sido rechazados. */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getContext(),getString(R.string.editacepted),Toast.LENGTH_LONG).show();
                actualizarDatosVista();
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(getContext(),getString(R.string.editcanceled),Toast.LENGTH_LONG).show();
            }
        }
    }

    /* Si hay una pelicula seleccionada, actualizar todos los datos de la vista. */
    public void actualizarDatosVista() {
        if(posicionPelicula != -1){
            textView_Director1.setVisibility(View.VISIBLE);
            textView_Ano1.setVisibility(View.VISIBLE);
            button_EditarPelicula.setVisibility(View.VISIBLE);
            button_PeliculaRelacionada.setVisibility(View.VISIBLE);

            textView_TituloPelicula.setText(FilmDataSource.films.get(posicionPelicula).getTitle());
            textView_Director2.setText(FilmDataSource.films.get(posicionPelicula).getDirector());
            textView_Ano2.setText(Integer.toString(FilmDataSource.films.get(posicionPelicula).getYear()));

            String formato = "";
            switch (FilmDataSource.films.get(posicionPelicula).getFormat()){
                case(Film.FORMAT_BLURAY):
                    formato = getString(R.string.bluray);
                    break;
                case(Film.FORMAT_DIGITAL):
                    formato = getString(R.string.digital);
                    break;
                case(Film.FORMAT_DVD):
                    formato = getString(R.string.dvd);
                    break;
            }

            String genero = "";
            switch (FilmDataSource.films.get(posicionPelicula).getGenre()){
                case(Film.GENRE_SCIFI):
                    genero = getString(R.string.scifi);
                    break;
                case(Film.GENRE_HORROR):
                    genero = getString(R.string.horror);
                    break;
                case(Film.GENRE_DRAMA):
                    genero = getString(R.string.drama);
                    break;
                case(Film.GENRE_COMEDY):
                    genero = getString(R.string.comedy);
                    break;
                case(Film.GENRE_ACTION):
                    genero = getString(R.string.action);
                    break;
            }
            textView_Descripcion.setText(formato + ", " + genero);
            textView_Notas.setText(FilmDataSource.films.get(posicionPelicula).getComments());
            imageView_Pelicula.setImageResource(FilmDataSource.films.get(posicionPelicula).getImageResId());
        }

    }

    /* Ocultar todos los datos de la vista. */
    public void ocultarVista(){
        textView_Director1.setVisibility(View.INVISIBLE);
        textView_Ano1.setVisibility(View.INVISIBLE);
        button_EditarPelicula.setVisibility(View.INVISIBLE);
        button_PeliculaRelacionada.setVisibility(View.INVISIBLE);

        textView_TituloPelicula.setText("");
        textView_Director2.setText("");
        textView_Ano2.setText("");
        textView_Descripcion.setText("");
        textView_Notas.setText("");
        imageView_Pelicula.setImageDrawable(null);
    }

    /* Cuando se pulsa el boton de volver, volver a cargar el fragment de la lista. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (android.R.id.home):
                /* Cargamos otra vez el fragmento anterior. */
                FilmListFragment filmListFragment = new FilmListFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container,filmListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
        }
        return super.onOptionsItemSelected(elemento);
    }

    /* Actualizar la posicion de la pelicula seleccionada desde otras acitividades. */
    public void setPosicionPelicula(int posicion){
        this.posicionPelicula = posicion;
    }
}
