package filmoteca.eps.ua.es.filmoteca_zayd.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.fragments.FilmDataFragment;
import filmoteca.eps.ua.es.filmoteca_zayd.fragments.FilmListFragment;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.Constants;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.GeofenceBroadcastReceiver;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.ObjectStreamManager;

import com.google.android.gms.ads.MobileAds;


public class FragmentContainerActivity extends AppCompatActivity implements FilmListFragment.OnItemSelectedListener {

    private static final String TAG = "FragmentContainer";
    private GeofencingClient gfc;
    private static ArrayList<Geofence> mGeofenceList;

    private enum PendingGeofenceTask {
        ADD, REMOVE, NONE
    }

    private PendingGeofenceTask mPendingGeofenceTask = PendingGeofenceTask.NONE;

    private PendingIntent mGeofencePendingIntent;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        try{
            boolean showAds= getIntent().getExtras().getBoolean("showAds", false);

            if(showAds){
                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId(Constants.AD_UNIT_ID);
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd.setAdListener(new AdListener(){
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mInterstitialAd.show();
                    }
                });
            }
        }catch(Exception e){

        }

        /* Lectura de ficheros, sincrona y asincrona. */
        //ObjectStreamManager.cargarDatosFichero(FragmentContainerActivity.this);
        new ObjectStreamManager.cargarDatosFicheroAsincrono(FragmentContainerActivity.this).execute();

        /* Comprobar si se esta usando el layout dinamico. */
        if (findViewById(R.id.fragment_container) != null) {
            /* Si se esta restaurando, no hace falta cargar el fragmento. */
            if (savedInstanceState != null)
                return;

            /* Creamos el fragmento. */
            FilmListFragment filmListFragment = new FilmListFragment();

            /* Pasamos los extras del intent al fragmento. */
            filmListFragment.setArguments(getIntent().getExtras());

            /* Añadimos el fragmento al contenedor. */
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, filmListFragment).commit();
        }


        FirebaseInstanceId.getInstance().getInstanceId().
                addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {

                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                    }
                });

        FirebaseMessaging.getInstance().subscribeToTopic("test")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        gfc = LocationServices.getGeofencingClient(this);
        // Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
        mGeofencePendingIntent = null;



        mGeofenceList = new ArrayList<Geofence>();

        if(!checkLocationPermission()){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }else{
            populateGeofences();
        }

    }

    public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (checkLocationPermission()) {
            populateGeofences();
        }else{
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    @Override
    public void onItemSelectedListener(int position) {
        FilmDataFragment filmDataFragment = (FilmDataFragment)this.getSupportFragmentManager().findFragmentById(R.id.data_fragment);

        if(filmDataFragment != null){
            /* Tipo estatico: actualizamos directamente el fragment. */
            filmDataFragment.setPosicionPelicula(position);
            filmDataFragment.actualizarDatosVista();
        }else{
            /* Tipo dinamico: hacemos transicion al nuevo fragment. */
            filmDataFragment = new FilmDataFragment();
            Bundle args = new Bundle();
            args.putInt("position",position);
            filmDataFragment.setArguments(args);

            FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container,filmDataFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /* Cuando se borre un elemento de la lista, notificamos a la vista de detalles que oculte la vista. */
    @Override
    public void onItemDeletedListener() {
        FilmDataFragment filmDataFragment = (FilmDataFragment)this.getSupportFragmentManager().findFragmentById(R.id.data_fragment);
        if(filmDataFragment != null){
            filmDataFragment.ocultarVista();
        }
    }

    public static void addToGeofenceList(Geofence mGeofence) {
        FragmentContainerActivity.mGeofenceList.add(mGeofence);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    populateGeofences();
                } else {
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    public void populateGeofences(){
        boolean populate = false;
        ArrayList<String> toRemoveList = new ArrayList<String>();
        for (Film film : FilmDataSource.films) {
            Geofence  geofence= new Geofence.Builder()
                    .setRequestId(film.getId())
                    .setCircularRegion(film.getLat(), film.getLng(), 500)
                    .setExpirationDuration(3600 * 1000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .setLoiteringDelay(3000)
                    .build();
            if (film.isGeofence()) {
                if (!mGeofenceList.contains(geofence)) {

                    mGeofenceList.add(geofence);
                    populate = true;
                }
            }else{
                if(mGeofenceList.contains(geofence)){
                    toRemoveList.add(geofence.getRequestId());
                }
            }
        }

        if(toRemoveList.size()>0 && gfc != null){
            gfc.removeGeofences(toRemoveList);
        }

        Toast.makeText(FragmentContainerActivity.this, "geofencelist added", Toast.LENGTH_SHORT).show();
        if(populate)
            gfc.addGeofences(getGeofencingRequest(), getGeofencePendingIntent()).addOnSuccessListener(this, new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
//                    Toast.makeText(FragmentContainerActivity.this, "geofences added", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, e.getMessage());
//                    Toast.makeText(FragmentContainerActivity.this, "geofence couldn't be added", Toast.LENGTH_SHORT).show();
                }
            });

    }
}
