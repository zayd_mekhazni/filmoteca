package filmoteca.eps.ua.es.filmoteca_zayd.thrash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.activities.AboutActivity;
import filmoteca.eps.ua.es.filmoteca_zayd.adapters.FilmArrayAdapter;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

public class FilmListActivity extends AppCompatActivity {

    private ListView listView_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);

        /* Asignar referencias de vistas. */
        listView_list = findViewById(R.id.listView_list);

        /* Inicializar adaptador. */
        final FilmArrayAdapter adaptador = new FilmArrayAdapter(this, R.layout.film_item_list,FilmDataSource.films);
        final int[] count = {0};
        listView_list.setAdapter(adaptador);

        /* Listener de la lista de peliculas. */
        listView_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(FilmListActivity.this,FilmDataActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_POSITION,position);
                startActivity(intent);
            }
        });

        /* Establecer el modo CHOICE_MODE_MULTIPLE_MODAL en el ListView, y su respectivo listener. */
        listView_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView_list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            /* Cuando se seleccione o se deseleccione un elemento, actualizar el contador. */
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if(checked){
                    count[0]++;
                }else{
                    count[0]--;
                }
                mode.setTitle(count[0] + getString(R.string.selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_contextual,menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            /* Cuando se pulsa el boton de Borrar, eliminar todos los elementos seleccionados, y guardar los cambios en el fichero. */
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_delete:
                        SparseBooleanArray selected = listView_list.getCheckedItemPositions();
                        for(int i =(selected.size() - 1); i >= 0; i--){
                            if(selected.valueAt(i)){
                                Film selectedFilm = adaptador.getItem(selected.keyAt(i));
                                adaptador.remove(selectedFilm);
                            }
                        }
                        ((BaseAdapter) listView_list.getAdapter()).notifyDataSetChanged();
                        count[0] = 0;
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) { }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_settings,menu);
        return true;
    }

    /* Comprobamos que elemento del menu fue seleccionado. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (R.id.menu_settings01):
                Film film = new Film();
                film.setTitle("Prueba");
                film.setDirector("Prueba");
                film.setYear(1111);
                film.setImdbUrl("http://www.google.es");
                film.setComments("Prueba");
                film.setGenre(Film.GENRE_SCIFI);
                film.setFormat(Film.FORMAT_DVD);
                film.setImageResId(R.mipmap.ic_launcher);
                FilmDataSource.films.add(film);
                ((BaseAdapter) listView_list.getAdapter()).notifyDataSetChanged();
                return true;
            case (R.id.menu_settings02):
                startActivity(new Intent(FilmListActivity.this,AboutActivity.class));
                return true;

        }
        /* Devolvemos false si no hemos hecho nada con el elemento
           seleccionado (permitira que se ejecuten otros manejadores)
           Si devolvemos true finalizara el procesamiento. */
        return false;
    }

}