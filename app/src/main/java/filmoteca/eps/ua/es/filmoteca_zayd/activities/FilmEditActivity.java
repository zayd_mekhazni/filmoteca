package filmoteca.eps.ua.es.filmoteca_zayd.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.model.LatLng;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.fragments.FilmDataFragment;
import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.ObjectStreamManager;

public class FilmEditActivity extends AppCompatActivity {

    private static final int ANADIR_POS = 9002;

    private LatLng point;

    /* Asignar referencias de vistas. */
    ImageView imageView_Editando;
    Button button_CapturarFotografia;
    Button button_SeleccionarImagen;
    EditText editText_EditandoTitulo;
    EditText editText_EditandoNombre;
    EditText editText_EditandoAno;
    EditText editText_EditandoEnlace;
    Spinner spinner_EditandoGenero;
    Spinner spinner_EditandoFormato;
    EditText editText_EditandoComentarios;
    Button button_EditandoGuardar;
    Button button_EditandoCancelar;
    Button button_EditPosicion;
    Switch switch_Geofence;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_edit);

        /* Activar el boton para volver a la pantalla principal. */
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /* Asignar referencias de vistas. */
        imageView_Editando = findViewById(R.id.imageView_Editando);
        button_CapturarFotografia = findViewById(R.id.button_CapturarFotografia);
        button_SeleccionarImagen = findViewById(R.id.button_SeleccionarImagen);
        editText_EditandoTitulo = findViewById(R.id.editText_EditandoTitulo);
        editText_EditandoNombre = findViewById(R.id.editText_EditandoNombre);
        editText_EditandoAno = findViewById(R.id.editText_EditandoAño);
        editText_EditandoEnlace = findViewById(R.id.editText_EditandoEnlace);
        spinner_EditandoGenero = findViewById(R.id.spinner_EditandoGenero);
        spinner_EditandoFormato = findViewById(R.id.spinner_EditandoFormato);
        editText_EditandoComentarios = findViewById(R.id.editText_EditandoComentarios);
        button_EditandoGuardar = findViewById(R.id.button_EditandoGuardar);
        button_EditandoCancelar = findViewById(R.id.button_EditandoCancelar);
        button_EditPosicion = findViewById(R.id.add_pos);
        switch_Geofence = findViewById(R.id.geofence);

        Intent intent = getIntent();
        final int posicionPelicula = intent.getIntExtra(FilmDataFragment.EXTRA_FILM_POSITION,0);
        imageView_Editando.setImageResource(FilmDataSource.films.get(posicionPelicula).getImageResId());
        editText_EditandoTitulo.setText(FilmDataSource.films.get(posicionPelicula).getTitle());
        editText_EditandoNombre.setText(FilmDataSource.films.get(posicionPelicula).getDirector());
        editText_EditandoAno.setText(Integer.toString(FilmDataSource.films.get(posicionPelicula).getYear()));
        editText_EditandoEnlace.setText(FilmDataSource.films.get(posicionPelicula).getImdbUrl());
        editText_EditandoComentarios.setText(FilmDataSource.films.get(posicionPelicula).getComments());

        switch_Geofence.setChecked(FilmDataSource.films.get(posicionPelicula).isGeofence());

        button_EditPosicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent posIntent = new Intent(FilmEditActivity.this, PositionActivity.class);

                startActivityForResult(posIntent, ANADIR_POS);
            }
        });

        switch (FilmDataSource.films.get(posicionPelicula).getFormat()){
            case(Film.FORMAT_BLURAY):
                spinner_EditandoFormato.setSelection(Film.FORMAT_BLURAY);
                break;
            case(Film.FORMAT_DIGITAL):
                spinner_EditandoFormato.setSelection(Film.FORMAT_DIGITAL);
                break;
            case(Film.FORMAT_DVD):
                spinner_EditandoFormato.setSelection(Film.FORMAT_DVD);
                break;
        }

        switch (FilmDataSource.films.get(posicionPelicula).getGenre()){
            case(Film.GENRE_SCIFI):
                spinner_EditandoGenero.setSelection(Film.GENRE_SCIFI - 3);
                break;
            case(Film.GENRE_HORROR):
                spinner_EditandoGenero.setSelection(Film.GENRE_HORROR - 3);
                break;
            case(Film.GENRE_DRAMA):
                spinner_EditandoGenero.setSelection(Film.GENRE_DRAMA - 3);
                break;
            case(Film.GENRE_COMEDY):
                spinner_EditandoGenero.setSelection(Film.GENRE_COMEDY - 3);
                break;
            case(Film.GENRE_ACTION):
                spinner_EditandoGenero.setSelection(Film.GENRE_ACTION - 3);
                break;
        }

        /* Listener del boton Guardar. */
        button_EditandoGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Film film = FilmDataSource.films.get(posicionPelicula);
                film.setTitle(editText_EditandoTitulo.getText().toString());
                film.setDirector(editText_EditandoNombre.getText().toString());
                film.setYear(Integer.valueOf(editText_EditandoAno.getText().toString()));
                film.setImdbUrl(editText_EditandoEnlace.getText().toString());
                film.setComments(editText_EditandoComentarios.getText().toString());
                film.setGeofence(switch_Geofence.isChecked());
                if (point != null){
                    film.setLat(point.latitude);
                    film.setLng(point.longitude);
                }

                switch (spinner_EditandoFormato.getSelectedItemPosition()){
                    case(Film.FORMAT_BLURAY):
                        film.setFormat(Film.FORMAT_BLURAY);
                        break;
                    case(Film.FORMAT_DIGITAL):
                        film.setFormat(Film.FORMAT_DIGITAL);
                        break;
                    case(Film.FORMAT_DVD):
                        film.setFormat(Film.FORMAT_DVD);
                        break;
                }

                switch (spinner_EditandoGenero.getSelectedItemPosition() + 3){
                    case(Film.GENRE_SCIFI):
                        film.setGenre(Film.GENRE_SCIFI);
                        break;
                    case(Film.GENRE_HORROR):
                        film.setGenre(Film.GENRE_HORROR);
                        break;
                    case(Film.GENRE_DRAMA):
                        film.setGenre(Film.GENRE_DRAMA);
                        break;
                    case(Film.GENRE_COMEDY):
                        film.setGenre(Film.GENRE_COMEDY);
                        break;
                    case(Film.GENRE_ACTION):
                        film.setGenre(Film.GENRE_ACTION);
                        break;
                }

                /* Escritura de ficheros, sincrona y asincrona. */
                //ObjectStreamManager.grabarDatosFichero(FilmEditActivity.this);
                new ObjectStreamManager.grabarDatosFicheroAsincrono(FilmEditActivity.this).execute();

                Intent resultIntent = new Intent();
                resultIntent.putExtra(FilmDataFragment.EXTRA_FILM_POSITION,posicionPelicula);
                if(switch_Geofence.isChecked() && point != null)
                    FragmentContainerActivity.addToGeofenceList(new Geofence.Builder()
                            .setRequestId(film.getId())
                            .setCircularRegion(film.getLat(), film.getLng(), 500)
                            .setExpirationDuration(3600 * 1000)
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL)
                            .setLoiteringDelay(3000)
                            .build());
                setResult(Activity.RESULT_OK,resultIntent);
                finish();
            }
        });

        /* Listener del boton Cancelar. */
        button_EditandoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ANADIR_POS) {

            if(resultCode == RESULT_OK){

                Double lat = data.getExtras().getDouble("lat");
                Double lng = data.getExtras().getDouble("lng");

                point = new LatLng(lat,lng);

            }

        }

    }

    /* Cuando se pulsa el boton de volver, volver a la actividad principal. */
    @Override
    public boolean onOptionsItemSelected(MenuItem elemento){
        super.onOptionsItemSelected(elemento);
        switch(elemento.getItemId()){
            case (android.R.id.home): // ID especial para el boton HOME
                NavUtils.navigateUpTo(FilmEditActivity.this,
                        new Intent(FilmEditActivity.this, FragmentContainerActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(elemento);
    }
}
