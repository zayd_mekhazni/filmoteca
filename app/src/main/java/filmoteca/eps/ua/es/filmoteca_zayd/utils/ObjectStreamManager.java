package filmoteca.eps.ua.es.filmoteca_zayd.utils;

import android.content.Context;
import android.os.AsyncTask;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;

import filmoteca.eps.ua.es.filmoteca_zayd.models.Film;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

public class ObjectStreamManager {

    /* Cargar datos desde un fichero binario. */
    private static void cargarDatosFichero(Context context){
        try {
            FileInputStream fis = context.openFileInput("peliculas.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            FilmDataSource.films.clear();
            /* Comentar esta linea de codigo si no se quieren cargar las peliculsa de ejemplo. */
            //FilmDataSource.cargarPeliculasEjemplo();
            while (true) {
                Film film = (Film) ois.readObject();
                FilmDataSource.films.add(film);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* Escribir datos en un fichero binario. */
    private static void grabarDatosFichero(Context context){
        try {
            FileOutputStream fos = context.openFileOutput("peliculas.dat",Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for(int i=0;i<FilmDataSource.films.size();i++){
                Film film = FilmDataSource.films.get(i);
                oos.writeObject(film);
            }
            fos.close();
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /* Cargar datos desde un fichero binario, de manera asincrona. */
    public static class cargarDatosFicheroAsincrono extends AsyncTask<Void, Void, Void> {

        private WeakReference<Context> contextReference;

        public cargarDatosFicheroAsincrono(Context context) {
            this.contextReference = new WeakReference<Context>(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cargarDatosFichero(contextReference.get());
            return null;
        }
    }

    /* Escribir datos en un fichero binario, de manera asincrona. */
    public static class grabarDatosFicheroAsincrono extends AsyncTask<Void, Void, Void> {

        private WeakReference<Context> contextReference;

        public grabarDatosFicheroAsincrono(Context context) {
            this.contextReference = new WeakReference<Context>(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            grabarDatosFichero(contextReference.get());
            return null;
        }
    }

}
