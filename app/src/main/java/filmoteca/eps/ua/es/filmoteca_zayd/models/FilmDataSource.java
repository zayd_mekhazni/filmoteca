package filmoteca.eps.ua.es.filmoteca_zayd.models;

import java.util.ArrayList;
import java.util.List;

import filmoteca.eps.ua.es.filmoteca_zayd.R;

/* Clase para guardar las peliculas. */
public class FilmDataSource {

    public static List<Film> films;
    static{
        films = new ArrayList<Film>();
    }

    /* Metodo para insertar algunas peliculas de ejemplo en la aplicacion. */
    public static void cargarPeliculasEjemplo(){
        Film film = new Film();
        film.setTitle("Regreso al futuro");
        film.setDirector("Robert Zemeckis");
        film.setImageResId(R.drawable.backtothefuture);
        film.setComments("Comentario de prueba.");
        film.setFormat(Film.FORMAT_DIGITAL);
        film.setGenre(Film.GENRE_SCIFI);
        film.setImdbUrl("https://www.imdb.com/title/tt0088763/");
        film.setYear(1985);
        film.setGeofence(true);
        film.setLat(1.02);
        film.setLng(0.02);
        films.add(film);

        Film film1 = new Film();
        film1.setTitle("Regreso al futuro 2");
        film1.setDirector("Robert Zemeckis");
        film1.setImageResId(R.drawable.backtothefuture2);
        film1.setComments("");
        film1.setFormat(Film.FORMAT_DIGITAL);
        film1.setGenre(Film.GENRE_SCIFI);
        film1.setImdbUrl("https://www.imdb.com/title/tt0088763/");
        film1.setYear(1985);
        film1.setGeofence(true);
        film1.setLat(1.12);
        film1.setLng(0.12);
        films.add(film1);

        Film film2 = new Film();
        film2.setTitle("Regreso al futuro 3");
        film2.setDirector("Robert Zemeckis");
        film2.setImageResId(R.drawable.backtothefuture3);
        film2.setComments("");
        film2.setFormat(Film.FORMAT_DIGITAL);
        film2.setGenre(Film.GENRE_SCIFI);
        film2.setImdbUrl("https://www.imdb.com/title/tt0088763/");
        film2.setYear(1985);
        film2.setGeofence(true);
        film2.setLat(1.22);
        film2.setLng(0.22);
        films.add(film2);

        Film film3 = new Film();
        film3.setTitle("El señor de los anillos:\nLa comunidad del anillo");
        film3.setDirector("Peter Jackson");
        film3.setImageResId(R.drawable.esdla);
        film3.setComments("");
        film3.setFormat(Film.FORMAT_DIGITAL);
        film3.setGenre(Film.GENRE_SCIFI);
        film3.setImdbUrl("https://www.imdb.com/title/tt0088763/");
        film3.setYear(1985);
        film3.setGeofence(false);
        film3.setLat(1.32);
        film3.setLng(0.32);
        films.add(film3);
    }
}
