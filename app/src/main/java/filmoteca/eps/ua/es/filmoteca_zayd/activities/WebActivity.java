package filmoteca.eps.ua.es.filmoteca_zayd.activities;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.utils.Constants;

public class WebActivity extends AppCompatActivity {

    OAuth1RequestToken requestToken = null;
    OAuth10aService service;
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);


        service = new ServiceBuilder(Constants.API_KEY)
                .apiSecret(Constants.API_SECRET)
                .callback(Constants.CALLBACKURL)
                .build(TwitterApi.instance());

        try {
             webview = findViewById(R.id.webView);

            String authUrl = new WebActivity.authUrl().execute().get();


            //attach WebViewClient to intercept the callback url
            webview.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    if (url.startsWith(Constants.CALLBACKURL)) {
                        Uri uri = Uri.parse(url);
                        String verifier = uri.getQueryParameter("oauth_verifier");
                        new OauthEnd().execute(verifier);
                        new sendTweet().execute();
                        return true;
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }
            });
            //send user to authorization page
            SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);

            if(settings.getString("accessToken", null) == null && settings.getString("accessSecret", null) == null)
                webview.loadUrl(authUrl);
            else
                new sendTweet().execute();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private class authUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                requestToken = service.getRequestToken();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return service.getAuthorizationUrl(requestToken);
        }

    }

    private class OauthEnd extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
            final SharedPreferences.Editor editor = settings.edit();

            final String verifier = params[0];
//            final Verifier v = new Verifier(verifier);
            Log.v("R", requestToken.getRawResponse());
            // Setup storage for access token
            final OAuth1AccessToken accessToken;
            try {
                accessToken = service.getAccessToken(requestToken, verifier);
                editor.putString("accessToken", accessToken.getToken());
                editor.putString("accessSecret", accessToken.getTokenSecret());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


            String androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            editor.putString("androidId", androidId);
            editor.apply();
            return null;
        }
    }

    private class sendTweet extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);

            OAuth1AccessToken newAccessToken = new OAuth1AccessToken(settings.getString("accessToken", null), settings.getString("accessSecret", null));


            String urlTweet = "https://api.twitter.com/1.1/statuses/update.json?status=";

            String[] films = getIntent().getExtras().getStringArray("favoriteFilms");

            if(films.length > 1){
                urlTweet += "me encantan";
            }else{
                urlTweet += "me encanta";
            }
            for (String film: films) urlTweet += " " + film;

            OAuthRequest request = new OAuthRequest(Verb.POST, urlTweet);
            service.signRequest(newAccessToken, request);
            //response = request.send();

            Response response = null;
            String body = null;
            try {
                response = service.execute(request);
                body = response.getBody();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return body;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject myJson = null;

                myJson = new JSONObject(result);

                String name = myJson.optString("screen_name");
                Log.v("name", name);

                // set info
                SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("screen_name", name);
                editor.apply();
                setResult(RESULT_OK, null);
                finish();

            } catch (JSONException e) {
                setResult(RESULT_CANCELED, null);
                finish();
            }
            setResult(RESULT_CANCELED, null);
            finish();
        }
    }

}