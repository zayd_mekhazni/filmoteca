package filmoteca.eps.ua.es.filmoteca_zayd.activities;

import android.content.Intent;
import android.graphics.Point;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import filmoteca.eps.ua.es.filmoteca_zayd.R;
import filmoteca.eps.ua.es.filmoteca_zayd.fragments.FilmDataFragment;
import filmoteca.eps.ua.es.filmoteca_zayd.models.FilmDataSource;

public class PositionActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng myPoint;
    int posicionPelicula = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button saveBtn = findViewById(R.id.saveBtn);

        Intent myIntent = getIntent();

        posicionPelicula = myIntent.getIntExtra(FilmDataFragment.EXTRA_FILM_POSITION,-1);
        if(posicionPelicula == -1) {
            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (myPoint != null) {
                        Intent myIntent = new Intent();
                        myIntent.putExtra("lat", myPoint.latitude);
                        myIntent.putExtra("lng", myPoint.longitude);
                        setResult(RESULT_OK, myIntent);
                        finish();
                    } else {
                        Toast.makeText(PositionActivity.this, getString(R.string.null_position), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            saveBtn.setVisibility(View.GONE);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(posicionPelicula == -1) {
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng point) {
                    mMap.clear();
                    myPoint = point;
                    mMap.addMarker(new MarkerOptions().position(point));
                }
            });
        }else {
            LatLng filmPos = new LatLng(FilmDataSource.films.get(posicionPelicula).getLat(), FilmDataSource.films.get(posicionPelicula).getLng());
            mMap.addMarker(new MarkerOptions()
                                .position(filmPos)
                                .title(FilmDataSource.films.get(posicionPelicula).getTitle())
                                .snippet(FilmDataSource.films.get(posicionPelicula).getDirector() + getString(R.string.en) + FilmDataSource.films.get(posicionPelicula).getYear()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(filmPos));
        }
    }
}
